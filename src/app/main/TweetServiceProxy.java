package app.main;

//import java.util.List;

import java.util.List;

import app.models.Tweet;
import app.models.Tweeter;
//import app.models.Tweeter;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
//import retrofit.http.DELETE;
//import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface TweetServiceProxy
{

  @POST("/api/tweeters/{id}/tweets")
  Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);
  
  @GET("/api/tweeters")
  Call<List<Tweeter>> getAllTweeters();

  @GET("/api/tweeters/{id}")
  Call<Tweeter> getTweeter(@Path("id") String id);

  @POST("/api/tweeters")
  Call<Tweeter> createTweeter(@Body Tweeter Tweeter);

  @DELETE("/api/tweeters/{id}")
  Call<Tweeter> deleteTweeter(@Path("id") String id);

  @DELETE("/api/tweeters")
  Call<String> deleteAllTweeters();

  @GET("/api/tweets")
  Call<List<Tweet>> getAllTweets();

  @DELETE("/api/tweets")
  Call<String> deleteAllTweets();

  @GET("/api/tweeters/{id}/tweets")
  Call<List<Tweet>> getTweets(@Path("id") String id);

  @GET("/api/tweeters/{id}/tweets/{tweetId}")
  Call<Tweet> getTweet(@Path("id") String id, @Path("id") String tweetId);

  @DELETE("/api/tweeters/{id}/tweets/{tweetId}")
  Call<Tweet> deleteTweet(@Path("id") String id, @Path("id") String id2);



}
