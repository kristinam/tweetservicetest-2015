package app.models;

import static com.google.common.base.MoreObjects.toStringHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import com.google.common.base.Objects;

public class Tweet
{

	  public String   id;
	  public String   message_text;
	  public String   contact;
	  public Long     date;

  public Tweet()
  {
  }

  public Tweet(String message_text, String contact)
  {
    this.id = UUID.randomUUID().toString();
    this.message_text = message_text;
    this.contact = contact;
    this.date = new Date().getTime();;
  }

  
  public String getDateString()
  {
    return formattedDate(new Date(date));
  }

 
  private String formattedDate(Date date)
  {
    DateFormat sdf = new SimpleDateFormat("EEE d MMM yyyy H:mm");
    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
    return sdf.format(date);
  }
  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Tweet)
    {
      final Tweet other = (Tweet) obj;
      return Objects.equal(id, other.id) 
          && Objects.equal(message_text, other.message_text) 
          && Objects.equal(contact, other.contact)
          && Objects.equal(date, other.date);
    }
    else
    {
      return false;
    }
  }
  @Override
  public String toString()
  {
     return toStringHelper(this)
         .addValue(id)
         .addValue(message_text)
         .addValue(contact)
         .addValue(date)
         .toString();

  }

}
