package app.models;


import com.google.common.base.Objects;
import static com.google.common.base.MoreObjects.toStringHelper;

import java.util.UUID;

public class Tweeter
{
  public String id;
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  
  public Tweeter(String firstName, String lastName, String email, String password)
  {
	this.id = UUID.randomUUID().toString();
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
  }
  
  public String toString()
  {
    return toStringHelper(this).addValue(id)
    		                   .addValue(firstName)
                               .addValue(lastName)
                               .addValue(password)
                               .addValue(email)                               
                               .toString();
  }
  
  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Tweeter)
    {
      final Tweeter other = (Tweeter) obj;
      return Objects.equal(id,  	   other.id) 
    	  && Objects.equal(firstName,  other.firstName) 
          && Objects.equal(lastName,   other.lastName)
          && Objects.equal(email,      other.email)
          && Objects.equal(password,   other.password);                               
    }
    else
    {
      return false;
    }
  } 
}