package app.test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import app.main.TweetServiceAPI;
import app.models.Tweeter;

public class TweeterTest
{
  static Tweeter tweeterArray [] = 
  { 
    new Tweeter ("homer",  "simpson", "homer@simpson.com",  "secret"),
    new Tweeter ("lisa",   "simpson", "lisa@simpson.com",   "secret"),
    new Tweeter ("maggie", "simpson", "maggie@simpson.com", "secret"),
    new Tweeter ("bart",   "simpson", "bart@simpson.com",   "secret"),
    new Tweeter ("marge",  "simpson", "marge@simpson.com",  "secret"),
  };
  List <Tweeter> tweeterList = new ArrayList<>();	
	
  private TweetServiceAPI tweetServiceAPI = new TweetServiceAPI();
	
  @Before
  public void setup() throws Exception
  { 
    for (Tweeter tweeter : tweeterArray)
    {
    	Tweeter returned = tweetServiceAPI.createTweeter(tweeter);
        tweeterList.add(returned);
    }
  }
  
  @After
  public void teardown() throws Exception
  {
	tweetServiceAPI.deleteAllTweeters();  
  }  
  
  @Test
  public void testCreate () throws Exception
  {
    assertEquals (tweeterArray.length, tweeterList.size());
    for (int i=0; i<tweeterArray.length; i++)
    {
      assertEquals(tweeterList.get(i), tweeterArray[i]);
    }
  }

  @Test
  public void testList() throws Exception
  {
    List<Tweeter> list = tweetServiceAPI.getAllTweeters();
    assertTrue (list.containsAll(tweeterList));
  }
  
  @Test
  public void testDelete () throws Exception
  {
    List<Tweeter> list1 = tweetServiceAPI.getAllTweeters();
    
    Tweeter testtweeter = new Tweeter("mark",  "simpson", "marge@simpson.com",  "secret");
    Tweeter returnedTweeter = tweetServiceAPI.createTweeter(testtweeter);
    
    List<Tweeter> list2 = tweetServiceAPI.getAllTweeters();
    assertEquals (list1.size()+1, list2.size());
    
    int code = tweetServiceAPI.deleteTweeter(returnedTweeter.id);
    assertEquals (200, code);
    
    List<Tweeter> list3 = tweetServiceAPI.getAllTweeters();
    assertEquals (list1.size(), list3.size());
  }
  
}
